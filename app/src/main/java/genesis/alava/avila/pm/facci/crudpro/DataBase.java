package genesis.alava.avila.pm.facci.crudpro;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DataBase extends SQLiteOpenHelper {

    private static int version = 1;
    private static String name = "Prueba" ;
    private static SQLiteDatabase.CursorFactory factory = null;

    public DataBase(Context context) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE RUTAS(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombre VARCHAR," +
                "categoria VARCHAR," +
                "protagonistas VARCHAR," +
                "transmision VARCHAR, " +
                "numero INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS SERIES";
        db.execSQL(sql);
        onCreate(db);
    }
    public void Insertar(String nombre, String categoria, String protagonistas, String transmision, String numero){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre", nombre);
        contentValues.put("categoria", categoria);
        contentValues.put("protagonistas", protagonistas);
        contentValues.put("transmision", transmision);
        contentValues.put("numero", numero);
        database.insert("SERIES",null,contentValues);
    }
    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("SERIES", null, null);
    }
    public void Modificar(String id, String nombre, String categoria, String protagonistas, String transmision, String numero){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre", nombre);
        contentValues.put("categoria", categoria);
        contentValues.put("protagonistas", protagonistas);
        contentValues.put("transmision", transmision);
        contentValues.put("numero", numero);
        database.update("SERIES", contentValues, "id= " + id, null);
    }
    public String consultarTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id","nombre", "categoria", "protagonistas", "transmision", "numero"};
        String sqlTable = "SERIES";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += "id " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "nombre " + c.getString(c.getColumnIndex("nombre")) + "\n" +
                        "categoria " + c.getString(c.getColumnIndex("categoria")) + "\n" +
                        "protagonistas " + c.getString(c.getColumnIndex("protagonistas")) + "\n" +
                        "transmision " + c.getString(c.getColumnIndex("transmision")) + "\n" +
                        "numero " + c.getString(c.getColumnIndex("numero")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
}

