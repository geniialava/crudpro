package genesis.alava.avila.pm.facci.crudpro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var nombre: EditText? = null
    private var categoria: EditText? = null
    private var protagonistas: EditText? = null
    private var transmision: EditText? = null
    private var numero: EditText? = null
    private var id: EditText? = null
    private var guardar: Button? = null
    private var consultar: Button? = null
    private var modificar: Button? = null
    private var eliminarT: Button? = null
    private var Datos: TextView? = null
    private var dataBase: DataBase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nombre = findViewById(R.id.nombre) as EditText
        categoria = findViewById(R.id.categoria) as EditText
        protagonistas = findViewById(R.id.protagonistas) as EditText
        transmision = findViewById(R.id.transmision) as EditText
        numero = findViewById(R.id.numero) as EditText
        id = findViewById(R.id.Id) as EditText
        guardar = findViewById(R.id.Guardar) as Button
        consultar = findViewById(R.id.consultar) as Button
        modificar = findViewById(R.id.modificar) as Button
        eliminarT = findViewById(R.id.EliminarT) as Button

        guardar!!.setOnClickListener(this)
        consultar!!.setOnClickListener(this)
        modificar!!.setOnClickListener(this)
        eliminarT!!.setOnClickListener(this)
        Datos = findViewById(R.id.Datos) as TextView

        dataBase = DataBase(this)
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.Guardar ->

                if (nombre!!.text.toString().isEmpty()) {

                } else if (nombre!!.text.toString().isEmpty()) {

                } else if (categoria!!.text.toString().isEmpty()) {

                } else if (protagonistas!!.text.toString().isEmpty()) {

                } else if (transmision!!.text.toString().isEmpty()) {

                } else if (numero!!.text.toString().isEmpty()) {

                } else {
                    dataBase!!.Insertar(
                        nombre!!.text.toString(), categoria!!.text.toString(),
                        protagonistas!!.text.toString(), transmision!!.text.toString(), numero!!.text.toString()
                    )
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show()
                    id!!.setText("")
                    nombre!!.setText("")
                    categoria!!.setText("")
                    protagonistas!!.setText("")
                    transmision!!.setText("")
                    numero!!.setText("")
                    Datos!!.text = ""
                }
            R.id.consultar -> {
                Datos!!.setText(dataBase!!.consultarTodos())
                id!!.setText("")
                nombre!!.setText("")
                categoria!!.setText("")
                protagonistas!!.setText("")
                transmision!!.setText("")
                numero!!.setText("")
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show()
            }

            R.id.modificar -> if (nombre!!.text.toString().isEmpty()) {

            } else if (nombre!!.text.toString().isEmpty()) {

            } else if (categoria!!.text.toString().isEmpty()) {

            } else if (protagonistas!!.text.toString().isEmpty()) {

            } else if (transmision!!.text.toString().isEmpty()) {

            } else if (numero!!.text.toString().isEmpty()) {

            } else if (id!!.text.toString().isEmpty()) {

            } else {
                dataBase!!.Modificar(
                    id!!.text.toString(), nombre!!.text.toString(), categoria!!.text.toString(),
                    protagonistas!!.text.toString(), transmision!!.text.toString(), numero!!.text.toString()
                )
                Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show()
                id!!.setText("")
                nombre!!.setText("")
                categoria!!.setText("")
                protagonistas!!.setText("")
                transmision!!.setText("")
                numero!!.setText("")
                Datos!!.text = ""
            }

            R.id.EliminarT -> {
                dataBase!!.EliminarTodo()
                Datos!!.text = ""
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }
}
